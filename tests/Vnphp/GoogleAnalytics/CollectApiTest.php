<?php

class CollectApiTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Vnphp\GoogleAnalytics\CollectApi
     */
    protected $instance;

    protected function setUp()
    {
        $this->instance = new \Vnphp\GoogleAnalytics\CollectApi('');
    }

    public function testTrackEvent()
    {
        $event = new \Vnphp\GoogleAnalytics\Model\CollectEvent('category', 'action', 'label');
        $this->instance->trackEvent($event);
    }
}
