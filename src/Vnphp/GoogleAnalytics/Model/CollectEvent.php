<?php


namespace Vnphp\GoogleAnalytics\Model;

class CollectEvent
{
    protected $category;

    protected $action;

    protected $label;

    protected $value = 0;

    protected $referer;

    protected $userIp;

    protected $userAgent;

    /**
     * @var string
     */
    protected $documentLocation;

    /**
     * CollectModel constructor.
     * @param $category
     * @param $action
     * @param $label
     */
    public function __construct($category, $action, $label)
    {
        $this->category = $category;
        $this->action = $action;
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return mixed
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * @return string
     */
    public function getDocumentLocation()
    {
        return $this->documentLocation;
    }

    /**
     * @param string $documentLocation
     *
     * @return $this
     */
    public function setDocumentLocation($documentLocation)
    {
        $this->documentLocation = $documentLocation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserIp()
    {
        return $this->userIp;
    }

    /**
     * @return mixed
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * @param mixed $referer
     *
     * @return $this
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * @param mixed $userIp
     *
     * @return $this
     */
    public function setUserIp($userIp)
    {
        $this->userIp = $userIp;

        return $this;
    }

    /**
     * @param mixed $userAgent
     *
     * @return $this
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return CollectEvent
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @param mixed $category
     * @return CollectEvent
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @param mixed $action
     * @return CollectEvent
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @param mixed $label
     * @return CollectEvent
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }
}
