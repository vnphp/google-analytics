<?php


namespace Vnphp\GoogleAnalytics;

use Buzz\Browser;
use Ramsey\Uuid\Uuid;
use Vnphp\GoogleAnalytics\Model\CollectEvent;

class CollectApi
{
    /**
     * @var Browser
     */
    protected $browser;

    protected $ua;

    /**
     * CollectApi constructor.
     * @param Browser $browser
     */
    public function __construct($ua, Browser $browser = null)
    {
        $this->ua = $ua;
        $this->browser = $browser ?: new Browser();
    }

    public function trackEvent(CollectEvent $model)
    {
        $data = [
            'v'   => 1,
            'tid' => $this->ua,
            'cid' => (string)Uuid::uuid4(),
            't'   => 'event',
        ];

        $data['ec'] = $model->getCategory();
        $data['ea'] = $model->getAction();
        $data['el'] = $model->getLabel();
        $data['ev'] = $model->getValue();
        $data['dr'] = $model->getReferer();
        $data['dl'] = $model->getDocumentLocation();
        $data['uip'] = $model->getUserIp();
        $data['userAgent'] = $model->getUserAgent();

        $content = http_build_query($data);

        $response = $this->browser->post('https://www.google-analytics.com/collect', [
            'Content-Length' => strlen($content),
        ], $content);

        return $response;
    }
}
