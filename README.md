# Vnphp Google Analytics


[![build status](https://gitlab.com/vnphp/google-analytics/badges/master/build.svg)](https://gitlab.com/vnphp/google-analytics/commits/master)


## Installation 

```
composer require vnphp/google-analytics
```

## Usage

```php
<?php

use \Vnphp\GoogleAnalytics\CollectApi;
use \Vnphp\GoogleAnalytics\Model\CollectEvent;

$api = new CollectApi('UA-...');
$event = new CollectEvent('category', 'action', 'label');
$api->trackEvent($event);
```